// Pra os interessados, o bot se chama @CodesignBot
const express = require('express');
const extractor = require('./keywordExtractor.js')
const bodyParser = require('body-parser')
const TelegramBot = require('node-telegram-bot-api');
const app = express();
const token = '<TOKEN>'
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

const pocLojas = [
	{
    "id": "1000",
    "area": "N/A",
    "ativo": true,
    "cnpj": "10.217.326/4353-54",
    "idContratante": "10",
    "contratante": "SCIC - SHOPPING CENTER IGUATEMI CAMPINAS",
    "dataFundacao": "2015-03-27 12:03:00",
    "fantasia": "BELUGA (Piso 1)",
    "razaoSocial": "LOJA 1000",
    "referencia": "N/A",
   	"tags": ['bolsa', 'perfume', 'joia', 'jóia', 'vestido', 'calca', 'calça' , 'camisa', 'camiseta', 'terno', 'sapato', 'fashion']
  },
  {
    "id": "1001",
    "area": "N/A",
    "ativo": true,
    "cnpj": "98.196.294/3921-49",
    "idContratante": "10",
    "contratante": "SCIC - SHOPPING CENTER IGUATEMI CAMPINAS",
    "dataFundacao": "2004-08-20 12:08:00",
    "fantasia": "NIKE (Piso 2)",
    "razaoSocial": "LOJA 1001",
    "referencia": "N/A",
    "tags": ['calcado', 'calçado', 'tenis', 'tênis', 'chinelo', 'camiseta', 'regata', 'luva', 'esporte']
  },
  {
    "id": "1002",
    "area": "N/A",
    "ativo": true,
    "cnpj": "23.459.689/9194-11",
    "idContratante": "10",
    "contratante": "SCIC - SHOPPING CENTER IGUATEMI CAMPINAS",
    "dataFundacao": "20(10-08-03 12:08:00",
        "fantasia": "LINDT (Piso 1)",
    "razaoSocial": "LOJA 1002",
    "referencia": "N/A",
    "tags": ['chocolate', 'doce', 'cafe', 'café', 'comida', 'bebida', 'bombom', 'sobremesa', 'sobremesas']
  }
]

app.post('/webhook', (req, res) => {  
 
  let body = req.body;

  // Checks this is an event from a page subscription
  if (body.object === 'page') {
    let msg = ''
    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function(entry) {
      let webhook_event = entry.messaging[0];
      msg = webhook_event.message
    });

  	checkProducts(msg).then(resText => {
  		res.status(200).send(resText);
  	}).catch(err => {
  		res.sendStatus(404);
  	})
  }

});

app.get('/webhook', (req, res) => {
  let VERIFY_TOKEN = "VNT"
    
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];
  
  if (mode && token) {
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);
    } else {
      res.sendStatus(403);      
    }
  }
});

app.listen(3000, (err) => {
	console.log('Rest server started at port 3000')
})

//não consegui fazer isso no bot do messenger do facebook, mas a idéia inicial era usar ele
const bot = new TelegramBot(token, {polling: true});
bot.on('message', (msg) => {
	chatId = msg.chat.id;

	checkProducts(msg.text).then(resText => {
		bot.sendMessage(chatId, resText);
	}).catch(err => {
		bot.sendMessage(chatId, 'Desculpe, não entendi o que deseja. Pode escrever novamente?')
	})
});

function checkProducts(msg) {
	return new Promise((fulfill, reject) => {
    extractor.extractKeywords(msg).then((splitedKeywords) => {
    	let lista = []
    	splitedKeywords.forEach((keyword) => {
    		pocLojas.forEach((loja) => {
    			if (loja["tags"].some(tag => tag == keyword)) {
    				lista.push(loja.fantasia)
    			}
    		})
    	})


    	let resText = "Não achamos lojas que trabalham com esses produtos 😕"
    	if (lista.length > 0) {
    		resText = "Achamos o produto que procura nas seguintes lojas:"
    		lista.forEach((elem) => {
    			resText = resText + "\n"
    			resText = resText + elem
    		})
    	}
    	fulfill(resText)
    }).catch((err) => {
    	reject(err)
    })
	})
}
