const NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');

const nlu = new NaturalLanguageUnderstandingV1({
	username: '<USERNAME>',
	password: '<PASSWORD>',
	version_date: NaturalLanguageUnderstandingV1.VERSION_DATE_2016_01_23
});

const prepositions = ['a', 'o', 'as', 'os', 'ante', 'após', 'apos', 'até', 'ate', 'com', 'contra', 'de', 'da', 'do', 'das', 
					'dos', 'desde', 'em', 'entre', 'para', 'per', 'perante', 'por', 'sem', 'sob', 'sobre', 'trás', 'tras']

function isPreposition(word) {
	return prepositions.some(preposition => preposition == word)
}

module.exports = {
	extractKeywords: function (msg) {
		return new Promise((fulfill, reject) => {
			let options = {
				text: '',
				features: {
					keywords: {}
				},
				language: 'pt-br'
			};

			options.text = msg

			nlu.analyze(options,(err, res) => {
				if (err) {
					reject(err)
				}

				if (res == null) {
					reject('Keyword not found!')
					return
				}

				let keywords = res.keywords
				let splitedKeywords = []

				keywords.map((keyword) => {
					let text = keyword.text
					let splitedText = text.split(/\s+/)
					splitedText.map((word) => {
						if(!isPreposition(word)) {
							splitedKeywords.push(word)
						}
					})
				})

				fulfill(splitedKeywords)
			});
		})
	}
}